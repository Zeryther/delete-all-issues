#!/bin/bash

TOKEN="TOKEN" # Your GitLab API token (create at https://gitlab.com/profile/personal_access_tokens this has to have the api scope!)
PROJECTID=0 # Your GitLab Project ID
MINIMUMISSUE=1 # The minimum issue ID to delete
MAXIMUMISSUE=10 # The maximum issue ID to delete

for (( i = $MINIMUMISSUE; i <= $MAXIMUMISSUE; i++ ))
do
        curl --request DELETE --header "PRIVATE-TOKEN: $TOKEN" https://gitlab.com/api/v4/projects/$PROJECTID/issues/$i
done